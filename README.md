# README #

Ejemplos que usan la interface sockets en Linux, basados en los ejemplos del capítulo de libro [Computer Systems: A Programmer's Perspective, 3/E](http://csapp.cs.cmu.edu/3e/home.html)

Hay problemas en el lazo while en echoserveri.c, sin importar que condicion se ponga el lazo no se rompe (aun usando break) por lo que no se logra enviarle el mensaje al cliente que se ha terminado de leer|enviar los datos del archivo y cerrar la conexion entre ambos . Ademas al enviar los datos del archivo la primera linea leida no es enviada al cliente.

### Integrantes ###
*	Kevin Palacios
*	Ivan Aguirre

### Compilación ###

* Compilar el cliente: 
	* make client

* Compilar el servidor:
	* make server

* Compilar todo:
	* make
	
### Uso ###
Ejecutar el cliente:

```
./client <host> <port>
```
Ejemplo:

```
./client 127.0.0.1 8080
```

Ejecutar el cliente:

```
./server <port>
```
Ejemplo:

```
./server 8080
```
