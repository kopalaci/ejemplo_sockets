#include "csapp.h"
#include <stdio.h>

void echo(int connfd);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		echo(connfd);
		Close(connfd);
	}
	exit(0);
}


typedef struct Linea
{
	struct  Linea *nextLine;
	struct  Linea *lastLine;
	char *content;
} Linea;
static unsigned get_file_size (const char * file_name)
{
    struct stat sb;
    if (stat (file_name, & sb) != 0) {
        fprintf (stderr, "'stat' failed for '%s': %s.\n",
                 file_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    return sb.st_size;
}

char *Strip(const char *s) {

	char *p = malloc(strlen(s) + 1);
	if(p) {
		char *p2 = p;
		while(*s != '\0') {
	    		if(*s != '\n') {
				*p2++ = *s++;
		    	} else {
				++s;
		    	}	
		}
		*p2 = '\0';

    	}
    	return p;
}
char *read_from_file(const char *filename)
{
    long int size = 0;
    FILE *file = fopen(filename, "r");

    if(!file) {
        printf("File error.\n");
        return NULL;
    }

    fseek(file, 0, SEEK_END);
    size = ftell(file);
    rewind(file);

    char *result = (char *) malloc(size);
    if(!result) {
        printf("Memory error.\n");
        return NULL;
    }

    if(fread(result, 1, size, file) != size) {
        printf("Read error.\n");
        return NULL;
    }

    fclose(file);
    return result;
}

void echo(int connfd)
{
	

	char buf[MAXLINE];
	char *texto=(char*)malloc(255*sizeof(char));
	int size;
	size_t n;
	rio_t rio;

	Rio_readinitb(&rio, connfd);
	char filename[MAXLINE];
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		buf[strlen(buf)-1] = '\0';
		if(strcmp(buf,"ok") == 0){
			
			char *result = read_from_file(filename);
			printf("%s",result);
			strcpy( buf, result );
			strcat( buf, "end" );
			rio_writen(connfd,buf, strlen(buf)+1);


		
		}else{
			printf("server received %lu bytes\n", n);
			strncpy(filename,buf,strlen(buf));
			size = get_file_size (buf);
			sprintf(texto, "%d", size);
			strcat( texto, "\n" );
			rio_writen(connfd,texto, sizeof(texto));
		}

		
		
	}
	/*while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		Linea *first_line = NULL;
		Linea *last_line = NULL;
		printf("server received %lu bytes\n", n);
		
		strcpy(buf2, buf);
		file=strtok(buf2, "\n");		
    		FILE *fp = fopen(file, "r");


		if (fp == NULL){
	    		printf("Error opening file!\n");
		}else{
			struct stat st;
			// stat() returns -1 on error. Skipping check in this example
			stat(file, &st);
			printf("File size: %d bytes\n", st.st_size);
			Rio_writen(connfd,st.st_size, strlen(st.st_size));
			
			char *lectura=(char*)malloc(255*sizeof(char));
			char* val;
			do{
				
				val = Fgets(lectura, sizeof(lectura), fp) ;
				Linea *linea = (Linea *)malloc(sizeof(Linea));
				printf("%s", lectura);	
				if(last_line == NULL){
					first_line = last_line = linea;
					last_line->nextLine=NULL;

				}
				else if(first_line != NULL){
					last_line->nextLine = linea;
					linea->lastLine = last_line;
					last_line = linea;
					last_line->nextLine=NULL;
				}
			
			
			}while(val != NULL);
			
			fclose(fp);
			printf("fin");	
			
			
		}
				
	}*/
	
}
