#include "csapp.h"
#include <unistd.h>

int main(int argc, char **argv)
{
	int clientfd;
	char *port;
	char *host, buf[MAXLINE], *filename;
	rio_t rio;

	if (argc != 4) {
		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];
	filename = argv[3];
	// se le concatena el fin de linea al nombre del archivo
	strcpy( buf, filename );
	strcat( buf, "\n" );

	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);
	Rio_writen(clientfd, buf, strlen(buf));
	Rio_readlineb(&rio, buf, MAXLINE);
	Fputs("Tamaño del archivo: ", stdout);
	buf[strlen(buf)-1] = '\0';
	Fputs(buf, stdout);
	Fputs(" bytes\n", stdout);

	Rio_writen(clientfd, "ok\n", 3);
	int val;
	while((val=Rio_readlineb(&rio, buf, MAXLINE))!= 0){
		printf("%d",val);
		FILE *fptr = fopen("salida.txt", "a+");
		fprintf(fptr,"%s",buf);
		fclose(fptr);

		
	}
	
	Fputs(" Archivo recibido\n", stdout);
	/*
	while(1){
		read = Rio_readlineb(&rio, buf, MAXLINE);

		Fputs(buf, stdout);
	}
	*/

	Close(clientfd);
	exit(0);
}
